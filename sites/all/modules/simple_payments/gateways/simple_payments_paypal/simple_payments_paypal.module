<?php

define('SIMPLE_PAYMENTS_PAYPAL_SUBMIT_URL', 'https://www.paypal.com/cgi-bin/webscr');
define('SIMPLE_PAYMENTS_PAYPAL_SUMBIT_URL_SANDBOX', 'https://www.sandbox.paypal.com/cgi-bin/webscr');
define('SIMPLE_PAYMENTS_PAYPAL_IPN_PATH', 'system/simple_payments_paypal/ipn');

/**
 * Creates a PayPal payment form.
 *
 * A button will still need to be added to the form before it can be used.
 *
 * @param array $vars
 *   An array of variables to be passed to PayPal.
 *   They can be PayPal variables from
 *     https://www.paypal.com/IntegrationCenter/ic_std-variable-ref-buy-now.html
 *   and the following:
 *     'uid' - The uid of the user who is making this payment (defaults to the
 *       current user).
 *     'nid' - The nid of the node this payment relates to.
 *     'module' - The module that should receive a callback when the payment is
 *       complete.
 *     'type' - Any payment subclassification the module wishes to use.
 *     'custom' - Data specific to the module / type, for example cart_id.
 *
 * @return array
 *   A generated FAPI form
 *
 * @see simple_payments_moneybookers_payment_form()
 */
function simple_payments_paypal_payment_form(array $vars = array()) {
  $vars['cmd'] = '_xclick';
  $vars['notify_url'] = url(SIMPLE_PAYMENTS_PAYPAL_IPN_PATH, array('absolute' => TRUE));
  if (!$vars['business']) {
    $vars['business'] = variable_get('simple_payments_paypal_account', '');
  }
  $form = simple_payments_build_form($vars);
  $form['#action'] = variable_get('simple_payments_paypal_sandbox', 0) ? SIMPLE_PAYMENTS_PAYPAL_SUMBIT_URL_SANDBOX : SIMPLE_PAYMENTS_PAYPAL_SUBMIT_URL;
  return $form;
}

/**
 * Implements hook_menu().
 */
function simple_payments_paypal_menu() {
  $items[SIMPLE_PAYMENTS_PAYPAL_IPN_PATH] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'simple_payments_paypal_ipn',
    'access callback' => TRUE
  );
  $items['admin/config/simple-payments/paypal'] = array(
    'title' => "PayPal",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('simple_payments_paypal_admin_form'),
    'description' => "Administer PayPal.",
    'access arguments' => array('administer Simple Payments'),
  );
  return $items;
}

/**
 * Form builder for the PayPal configuration form.
 *
 * @ingroup forms
 */
function simple_payments_paypal_admin_form(array $form_state) {
  $form['simple_payments_paypal_account'] = array(
    '#type' => 'textfield',
    '#title' => t("Default account"),
    '#default_value' => variable_get('simple_payments_paypal_account', ''),
    '#description' => t("Email address of receivers paypal account."),
  );
  $form['simple_payments_paypal_sandbox'] = array(
    '#type' => 'radios',
    '#title' => t("Sandbox/development mode"),
    '#description' => t("When in development mode, the payment gateway will point at sandbox.paypal.com for testing purposes."),
    '#options' => array(0 => t("Off"), t("On")),
    '#default_value' => variable_get('simple_payments_paypal_sandbox', 0),
  );
  return system_settings_form($form);
}

function _simple_payments_paypal_post(array $data = array()) {
  $post = '';
  foreach ($data as $key => $value) {
    $post .= $key. '='. urlencode($value). '&';
  }
  $post .= 'cmd=_notify-validate';
  return $post;
}

function _simple_payments_paypal_ipn_verify(array $vars = array()) {
  if (variable_get('simple_payments_paypal_sandbox', 0)) {
    // PayPal sandbox requires login in order to even access, so for sandbox
    // mode, simply return true.
    return TRUE;
  }
  $ch = curl_init(SIMPLE_PAYMENTS_PAYPAL_SUBMIT_URL);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, _simple_payments_paypal_post($vars));
  ob_start();
  if (curl_exec($ch)) {
    $info = ob_get_contents();
    curl_close($ch);
    ob_end_clean();
    if (eregi('VERIFIED', $info)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
  else {
    watchdog('simple_payments_paypal', "Call to curl_exec() failed. url=@url vars=@vars", array(
      '@vars' => print_r($vars, TRUE)
      ), WATCHDOG_ERROR);
    return FALSE;
  }
}

/**
 * Handles an incoming PayPal IPN.
 */
function simple_payments_paypal_ipn() {
  $ipn = $_POST;
  if (!_simple_payments_paypal_ipn_verify($ipn)) {
    return;
  }
  if ($ipn['payment_status'] != 'Completed' || empty($ipn['txn_id']) || empty($ipn['receiver_email'])) {
    return;
  }
  // Extracts payment variables that were encoded in the 'custom' field.
  $payment = simple_payments_explode_custom($ipn['custom']);
  $payment['gateway'] = 'paypal';
  $payment['transaction_id'] = $ipn['txn_id'];
  $payment['currency'] = $ipn['mc_currency'];
  $payment['amount'] = $ipn['mc_gross'];
  $payment['timestamp'] = strtotime($ipn['payment_date']);
  $payment['details'] = $ipn;
  $payment['payer_reference'] = $ipn['payer_email'];
  $payment['payee_reference'] = $ipn['receiver_email'];
  simple_payments_payment_received($payment);
}

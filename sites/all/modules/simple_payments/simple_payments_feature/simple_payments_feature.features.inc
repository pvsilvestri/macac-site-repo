<?php
/**
 * @file
 * simple_payments_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function simple_payments_feature_node_info() {
  $items = array(
    'payment' => array(
      'name' => t('Payment'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'payment_destination' => array(
      'name' => t('Payment Destination'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

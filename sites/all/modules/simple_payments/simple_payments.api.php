<?php

/**
 * @file
 * Hooks provided by Simple Payments.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Passes a completed payment to the module that created it for processing.
 *
 * @param array $payment
 *   A payment to be processed.
 *
 * @return bool
 *   TRUE if the payment was successfully processed.
 */
function hook_simple_payment_process(array $payment) {
}
